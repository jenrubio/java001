package deloitte.Academy.lesson01.LogicalOperators;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class helps to explain how the Logical Operators (and, or, xor and not) work. 
 * Logical Operators: Expressions that result in Boolean values.
 * Also, we can see some examples of Logger and level objects.
 * @author jenrubio
 *
 */

public class LogicalOperators {
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());
	public boolean result = false;
	
	/**
	 * In this example, the condition is going to be true if:
	 * @param value1: Integer
	 * @param value2: Integer
	 * @return true if (value2 >10) && (value1 < 10) this happens and false if one or both of them aren't true
	 */
	public boolean and(int value1, int value2) {
		try {
			if((value1 < 10) && (value2 > 10)) {
				result = true;
			}
			else {
				result = false;
			}
			LOGGER.info("LogicalOperators - AND: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		
		return result;
	}
	
	/**
	 * 
	 * @param value1 < 10 or
	 * @param value2 > 10
	 * @return true if this happens or false if both don't accomplish the conditions.
	 */
	public boolean or(int value1, int value2) {
		try {
			if((value1 < 10) || (value2 > 10)) {
				result = true;
			}
			else {
				result = false;
			}
			LOGGER.info("Logical Operators - OR: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		
		return result;
	}
	
	/**
	 * XOR operator.
	 * @param value1 == 10 xor
	 * @param value2 < 10
	 * @return: result = true if one of them aren't true and result = false if both of them are true or false.
	 */
	public boolean xor(int value1, int value2) {
		try {
			if((value1 == 10) ^ (value2 < 10)) {
				result = true;
			}
			else {
				result = false;
			}
			LOGGER.info("Logical Operators - NOT: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		
		return result;
	}
	
	/**
	 * NOT value == 10 (if value == 10 result = false)
	 * @param value1 
	 * @return result.
	 */
	public boolean not(int value1) {
		try {
			if(!(value1 == 10)) {
				result = false;
			}
			else {
				result = true;
			}
			LOGGER.info("Logical Operators - XOR: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return result;
	}
	
}
