package deloitte.Academy.lesson01.Comparisons;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class presents some examples about the comparisons: 
 * These operators are used in expressions that return the Boolean values true or false
 * Equal to, not equal to, less than, greater than, less or equal than and greater or equal than.
 * @author jenrubio
 *
 */
public class Comparisons {
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());
	public Integer equalResult = 0;
	public Integer notEqualResult = 0;
	public Integer lessThan = 0;
	public Integer greaterThan = 0;
	public Integer lessEqual = 0;
	public Integer greaterEqual = 0;
	

	/**
	 * Method that determines if the two values are equals or not. 
	 * If they are, the result is going to be 1, and if they aren't, it will be 2
	 * @param value1 = must be Integer
	 * @param value2 = must be Integer
	 * @return equalResult = (Integer) gives the result of the comparison between value1 and value2 (equal)
	 */
	public Integer equal(Integer value1, Integer value2){
		try {
			if(value1 == value2) {
				equalResult = 1;
			}
			else {
				equalResult = 2;
			}
			LOGGER.info("Comparison - Equal: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		
		return equalResult;
	}
	
	/**
	 * Method that determines if value1 is not equal to 6. 
	 * @param value1 and value2 = need to be Integer
	 * @return notEqualResult
	 * value2 not equal to value1 = 1
	 * value2 equal to value1 -- result = 2
	 */
	public Integer notEqual(int value1, int value2) {
		try {
			if(value2 != value1) {
				notEqualResult = 1;
			}
			else {
				notEqualResult = 2;
			}
			LOGGER.info("Comparison - not Equal: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return notEqualResult;
	}
	
	
	/**
	 * Method that determines if value2 is less than 6. 
	 * @param value2: must be Integer
	 * @return lessThan: value2 less than 6 -- result = 1; 
	 * value2 not less than 6 -- result = 2
	 */
	public Integer lessThan(int value2) {
		try {
			if(value2 < 6) {
				lessThan = 1;
			}
			else {
				lessThan = 2;
			}
			LOGGER.info("Comparison - Less than: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return lessThan;
	}
	
	/**
	 * Method that determines if value2 is greater than 6. 
	 * @param value2: must be Integer
	 * @return lessThan: value2 greater than 6 -- result = 1; 
	 * value2 not greater than 6 -- result = 2
	 */
	public Integer greaterThan(int value1) {
		try {
			if(value1 > 6) {
				greaterThan = 1;
			}
			else {
				greaterThan = 2;
			}
			LOGGER.info("Comparison - Greater than: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return greaterThan;
	}
	
	/**
	 * Method that determines if the value1 is less equal than value2
	 * @param value1: must be Int
	 * @param value2: must be Int
	 * @return lessEqual:
	 * lessEqual = 1 -- Value1 is less than or equal than value2
	 * lessEqual = 2 -- value1 is bigger than value2
	 */
	public Integer lessEqual (int value1, int value2) {
		try {
			if(value1 <= value2) {
				lessEqual = 1;
			}
			else {
				lessEqual = 2;
			}
			LOGGER.info("Comparison - Less than or Equal than: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return lessEqual;
	}
	
	/**
	 * 
	 * @param value1: must be Int
	 * @param value2: must be Int
	 * @return greaterEqual:
	 * greaterEqual = 1 (value1 is bigger or equal than value2)
	 * greaterEqual = 2 (value1 is smaller than value2)
	 */
	public Integer greaterEqual(int value1, int value2) {
		try {
			if(value1 >= value2) {
				greaterEqual = 1;
			}
			else {
				greaterEqual = 2;
			}
			LOGGER.info("Comparison - Greater than or Equal than: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return greaterEqual;
	}

}
