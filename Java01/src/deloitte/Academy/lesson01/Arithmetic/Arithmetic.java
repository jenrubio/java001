package deloitte.Academy.lesson01.Arithmetic;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class has a lot of arithmetic examples: addition, subtraction, multiplication, division and modulus.
 * Each operator takes two operands, one on each side of the operator.
 * The entrance values must be integer for all the cases but the results can be integer or float. 
 * @author jenrubio
 *
 */
public class Arithmetic {
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());
	public Integer result = 0;
	public float result2 = 0;
	
	/**
	 * Addition: Method that add valor1 to valor2 and returns that value. 
	 * If the addition can not be completed the LOGGER sends a exception.
	 * @param value1: must be int
	 * @param value2: must be int
	 * @return result = (Integer) gives the result of the addition between valor1 and valor2
	 */
	public Integer addition(Integer value1, Integer value2) {
		try {
			result = value1 + value2;
			LOGGER.info("Addition: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}

		return result;
	}
	
	/**
	 * Subtraction: Method that subtract valor1 to valor2 and returns that result. 
	 * If the subtraction can not be completed the LOGGER sends a exception.
	 * @param value1: must be int
	 * @param value2: must be int
	 * @return result = (Integer) returns the result of subtract valor2 from valor1
	 */
	public Integer subtraction(Integer value1, Integer value2) {
		try {
			result = value1 - value2;
			LOGGER.info("Subtraction: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return result;
	}
	
	
	/**
	 * Multiplication: Method that multiply valor1 and valor2
	 * If the multiplication can not be completed the LOGGER sends a exception.
	 * @param valor1: must be int
	 * @param valor2: must be int
	 * @result resultado = (Integer) returns the result of the multiplication between valor1 and valor2
	 */
	public Integer multiplication(Integer value1, Integer value2) {
		try {
			result = value1 * value2;
			LOGGER.info("Multiplication: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return result;
	}
	

	/**
	 * Division: Method that divide valor1 and valor2
	 * If the division can not be completed the LOGGER sends a exception.
	 * @param value1: must be int
	 * @param value2: must be int
	 * @return result2 = (Float) the division between valor1 and valor2
	 */
	public float division(Integer value1, Integer value2) {
		try {
			result2 = (float)value1 / (float)value2;
			LOGGER.info("Division: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return result2;
	}

	
	/**
	 * Modulus: Method that obtains the modulus of valor1 % valor2
	 * If the modulus can not be completed the LOGGER sends a exception.
	 * @param value1: must be int
	 * @param value2: must be int
	 * @return result2 = (Float) the modulus of valor1 and valor2
	 */
	public float modulus(Integer value1, Integer value2) {
		try {
			result2 = (float)value1 % (float) value2;
			LOGGER.info("Modulus: OK");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return result2;
	}

}
