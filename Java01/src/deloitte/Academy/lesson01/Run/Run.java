package deloitte.Academy.lesson01.Run;

import deloitte.Academy.lesson01.Arithmetic.Arithmetic;
import deloitte.Academy.lesson01.Comparisons.Comparisons;
import deloitte.Academy.lesson01.LogicalOperators.LogicalOperators;

/**
 * Project that makes some arithmetic, logical and comparisons operations.
 * You need to select the option (1 = Arithmetic, 2 = Comparisons and 3 = Logical Operators)
 * Also, you can choose the values for value1 and value2 variables. 
 * @author jenrubio
 */

public class Run {

	public static void main(String[] args) {
		/**
		 * Variables for the different results.
		 */
		Integer result = 0;
		float result2 = 0;
		boolean result3 = false;
		
		/**
		 * Values that are necessary to obtain the results.
		 * The user can change those values for different results.
		 */
		Integer value1 = 10;
		Integer value2 = 8;
		
		/*
		 * This option is going to chance the type of operations that the program is going to be doing.
		 * Option = 1 -- Arithmetic
		 * Option = 2 -- Comparisons
		 * Option = 3 -- LogicalOperators
		 */
		Integer option = 2;
		

		Arithmetic ari = new Arithmetic();
		Comparisons compar = new Comparisons();
		LogicalOperators logic = new LogicalOperators();
		

		if(option == 1) {
			
			//Addition
			result = ari.addition(value1, value2);
			System.out.println("The addition between " +value1 +" and " + value2 + " is: " + result);
			
			//Subtraction
			result = ari.subtraction(value1, value2);
			System.out.println("The subtraction between " +value1 +" and " + value2 + " is: " + result);
			
			//Multiplication
			result = ari.multiplication(value1, value2);
			System.out.println("The multiplication between " +value1 +" and " + value2 + " is: " + result);
			
			//Division
			result2 = ari.division(value1, value2);
			System.out.println("The subtraction between " +value1 +" and " + value2 + " is: " + result2);
			
			//Modulus
			result2 = ari.modulus(value1, value2);
			System.out.println("The modulus of " +value1 +" and " + value2 + " is: " + result2);
			
		}
		else if(option == 2) {
			
			//Equal
			result = compar.equal(value1, value2);
			if(result == 1) {
				System.out.println("The values: " + value1 + " and "+ value2 +" are equals");
			}
			else {
				System.out.println("The values: " + value1 + " and " + value2 + " are not equals");
			}
			
			//Not equal
			result = compar.notEqual(value1, value2);
			if(result == 1) {
				System.out.println(value2 + " is not equal to " + value1);
			}
			else {
				System.out.println(value2 + " is equal to " + value1);
			}
			
			//Less than
			result = compar.lessThan(value2);
			if(result == 1) {
				System.out.println("The value: " + value2 + " is less than 6");
			}
			else {
				System.out.println("The value: " + value2 + " is bigger than 6");
			}
			
			//Greater than
			result = compar.greaterThan(value1);
			if(result == 1) {
				System.out.println("The value: " + value1 + " is greater than 6");
			}
			else {
				System.out.println("The value: " + value2 + " is smaller than 6");
			}
			
			//Less or equal than
			result = compar.lessEqual(value1, value2);
			if(result == 1) {
				System.out.println(value1 + " is less or equal than: " + value2);
			}
			else {
				System.out.println(value1 + " is bigger than: " + value2);
			}
			
			//Greater or equal than
			result = compar.greaterEqual(value1, value2);
			if(result == 1) {
				System.out.println(value1 + " is greater or equal than: " + value2);
			}
			else {
				System.out.println(value1 + " is smaller than: " + value2);
			}
		}
		
		else {
			//AND
			result3 = logic.and(value1, value2);
			if(result3 == true) {
				System.out.println(value1 + " is smaller than 10 and " + value2 +" is bigger than 10");
			}
			else {
				System.out.println(value1 + " and/or " + value2 + " don't meet the conditions");
			}
			
			//OR
			result3 = logic.or(value1, value2);
			if(result3 == true) {
				System.out.println(value1 + " is smaller than 10 or " + value2 +" is bigger than 10");
			}
			else {
				System.out.println(value1 + " and " + value2 + " don't meet the conditions.");
			}
			
			//XOR
			result3 = logic.xor(value1, value2);
			if(result3 == true) {
				System.out.println("TRUE: " + value1 + " == 10 xor " + value2 + " < 10");
			}
			else {
				System.out.println("FALSE: " + value1 + " == 10 xor " + value2 + " < 10");
			}
			
			//NOT
			result3 = logic.not(value1);
			if(result3 == true) {
				System.out.println(value1 + " is equal to 10");
			}else {
				System.out.println(value1 + " isn't equal to 10");
			}
		}
		
	
		
	
	}

}
